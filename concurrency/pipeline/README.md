# Pipeline

Say, we have to implement a data pipeline in which we have multiple stages, which needs to be run in sequence for each of the input.

Each stage can have multiple workers which does the job based on the corresponding workers' availability.

```mermaid
flowchart LR
    A["`Input as an array`"]
    B["`**Stage 1**
    ----
    Worker 1
    Worker 2`"]
    C["`**Stage 2**
    ----
    Worker 1
    Worker 2
    Worker 3`"]
    D["`Wait for all stages to be
    processed for all input`"]
    E[Exit]
    A --> B
    B --> C
    C --> D
    D --> E
```

package main

import (
	"fmt"
	"log"
	"time"
)

func main() {
	tasks := []Stage1Task{
		{url: "a"},
		{url: "b"},
		{url: "c"},
		{url: "d"},
		{url: "e"},
		{url: "f"},
		{url: "g"},
	}
	now := time.Now()
	defer func() {
		fmt.Printf("Time taken: %vs", time.Since(now).Seconds())
	}()
	res := fanOut(tasks, 2)
	log.Println("Result channel count ", len(res))
	for n := range fanin(res) {
		log.Printf("Time taken: %vs", time.Since(now).Seconds())
		log.Printf("Result: %v", n.url)
	}
}

package main

import (
	"log"
	"sync"
)

func fanin(res []<-chan Stage1Result) <-chan Stage1Result {
	var wg sync.WaitGroup

	out := make(chan Stage1Result)

	// Start an output goroutine for each input channel in res.  output
	// copies values from c to out until c is closed, then calls wg.Done.
	output := func(c <-chan Stage1Result, i int) {
		for n := range c {
			out <- n
		}
		log.Printf("Waitgroup done for worker %v", i)
		wg.Done()
	}
	log.Println("Adding wg by ", len(res))
	wg.Add(len(res))

	for i, c := range res {
		go output(c, i)
	}

	// Start a goroutine to close out once all the output goroutines are
	// done.  This must start after the wg.Add call.
	go func() {
		log.Println("Waiting for Waitgroup to close")
		wg.Wait()
		close(out)
		log.Println("Waitgroup closed")
	}()

	return out
}

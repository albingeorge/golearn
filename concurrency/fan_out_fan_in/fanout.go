package main

import (
	"log"
	"math/rand"
	"time"
)

type Stage1Task struct {
	url string
}

type Stage1Result struct {
	url string
}

func generateStage1InputChannel(nums ...Stage1Task) <-chan Stage1Task {
	out := make(chan Stage1Task)
	go func() {
		for _, n := range nums {
			out <- n
		}
		close(out)
	}()
	return out
}

func stage1Worker(id int, tasks <-chan Stage1Task) <-chan Stage1Result {
	log.Printf("stage1Worker; workerId: %v", id)
	out := make(chan Stage1Result)
	go func() {
		for task := range tasks {
			out <- stage1Process(task, id)
		}
		log.Printf("stage1Worker closing; workerId: %v", id)
		close(out)
	}()
	return out
}

// Simulates a long running task with arbitrary runtime
func stage1Process(t Stage1Task, workerId int) Stage1Result {
	sl := rand5()
	log.Printf("stage1Process worker Id: %v; Task: %v; Sleep: %vs", workerId, t.url, sl)
	time.Sleep(time.Duration(sl) * time.Second)
	return Stage1Result{url: t.url + t.url}
}

func fanOut(tasks []Stage1Task, workerCount int) []<-chan Stage1Result {
	c := generateStage1InputChannel(tasks...)
	resultsChannel := make([]<-chan Stage1Result, 0, workerCount)
	log.Println("fanout len res channel: ", len(resultsChannel))
	for i := 1; i <= workerCount; i++ {
		resultsChannel = append(resultsChannel, stage1Worker(i, c))
	}
	log.Println("fanout len res channel: ", len(resultsChannel))
	return resultsChannel
}

func rand5() int {
	return 1 + rand.Intn(5)
}

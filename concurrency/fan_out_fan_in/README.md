# Fan-out, Fan-in pattern

Multiple functions can read from the same channel until that channel is closed; this is called fan-out. This provides a way to distribute work amongst a group of workers to parallelize CPU use and I/O.

A function can read from multiple inputs and proceed until all are closed by multiplexing the input channels onto a single channel that’s closed when all the inputs are closed. This is called fan-in.

```mermaid
flowchart LR
    A["`Input an array of elements`"]
    B["`**Generator**
    Creates a read only input channel`"]
    C["`**Fan-out**
    Generates a number of workers all of which reads from the input channel`"]
    D[Worker 1]
    E[Worker 2]
    F[Worker 3]
    G["`**Fan-in**
    Merges the output from all workers into a single channel and closes this channale once all the workers are done processing`"]
    A --> B
    B --> C
    C --> D
    C --> E
    C --> F
    D --> G
    E --> G
    F --> G
    
    subgraph Worker pool
        D
        E
        F
    end
```
